import { useContext, useState, useEffect } from "react";
import {Accordion, Button} from "react-bootstrap";
import {Navigate, useNavigate, Link} from "react-router-dom";
import UserContext from "../UserContext";

export default function AllOrders(){
	const {user} = useContext(UserContext);
	const navigate = useNavigate();
	const [allOrders, setAllOrders] = useState([]);
	const [indOrder, setIndOrder] = useState([]);
	// const [prod, setProd] = useState("");
		const fetchData = () =>{
			fetch(`${process.env.REACT_APP_API_URL}/customers/getAllOrders`,{
				headers:{
					"Authorization": `Bearer ${localStorage.getItem("token")}`
				}
			})
			.then(res => res.json())
			.then(data => {
				console.log(data);


				setAllOrders(data.map(user => {

					return(


						<Accordion key={user._id}>
						      <Accordion.Item eventKey={user._id}>
						        <Accordion.Header>Orders from {user.firstName} {user.lastName} ({user._id})</Accordion.Header>
						        <Accordion.Body>
						         {user.order.map(list=>{

						         	return(
						         	<div key={list._plantId}> 
						         		<h6> Purchase Date: {list.orderedOn}</h6>
						         		<p>{list.plantName} - Quantity: {list.quantity}</p>
						         		<h6>Total Amount: {list.totalAmount}</h6>
						         		<hr />


						         	</div>
						         	)
						         })}
						        </Accordion.Body>
						      </Accordion.Item>
						</Accordion>
					
					)
				}))

			})
		}
	useEffect(()=>{
			
			fetchData();
		}, [])


	return (
			(user.isAdmin)
			?
			<>
				<div className="mt-5 mb-3 text-center">
					<h1>Order History</h1>
					
				</div>

				<div>
					{allOrders}

				</div>

				<br/>


			<Button as={Link}to="/admin" size="sm" variant="success">Back to Admin Dashboard</Button>
				

			</>
			:
			<Navigate to="/products" />

		)
}
