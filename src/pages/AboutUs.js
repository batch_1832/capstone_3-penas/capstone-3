import {Figure, Button} from "react-bootstrap"
import {Link} from "react-router-dom";


export default function aboutUs() {
  return (

<div className="text-center">
<h1 className="my-3">About Us</h1>

<br/>
    <Figure id="plant" >

     < img src="Cristina.jpg" />
      <h5 className="my-4">
       Penas garden was conceptualized by Cristina. it’s her love of gardening which leads her to a decision to open her hobby for business. There are various plants that everyone can choose from, from ornaments to garden vegetables. There are also garden needs and equipment that can be bought through this online shop like garden soil, fertilizer, pots, garden containers, etc...
      </h5>
    </Figure>

    <Button as={Link}to="/" size="sm" className="text-center" variant="success" >Back to Home Page</Button>

  </div>
  );
}
