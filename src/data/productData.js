const productData = [

	{
        id: "PD001",
        kind: "Garden crop",
        variety: "Lettuce",
        description: "A beautiful flower with variety of colors perfect for flower arrangeme...",
        price: 56,
        stocks: 8,
        isAvailable: true,
        
    },
{
        id: "PD002",
        kind: "Ornaments",
        variety: "fern",
        description: "A beautiful flower withvarietyof colors perfect for flower arrangeme...",
        price: 78,
        stocks: 7,
        isAvailable: true,
        
    },

    {
        id: "PD003",
        kind: "Garden products",
        variety: "garden soil",
        description: "full of rich nutrients garden soil",
        price: 105,
        stocks: 4,
        isAvailable: true,
        
    },

]

export default productData